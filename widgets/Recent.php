<?php

namespace app\widgets;

use yii\base\Widget;
use app\models\Article;

class Recent extends Widget {
  public $limit=3;

  function run()
  {
    $recent = Article::find()->select(['id','title','date','image'])->limit($this->limit)->orderBy('date desc')->all();
    return $this->render('recent', compact ('recent'));
  }
}
?>
