<?php

namespace app\widgets;

use yii\base\Widget;
use app\models\Article;

class Popular extends Widget {
  public $limit=3;

  function run()
  {
    $popular = Article::find()->select(['id','title','date','image'])->limit($this->limit)->orderBy('viewed desc')->all();
    return $this->render('popular', compact ('popular'));
  }
}
?>
