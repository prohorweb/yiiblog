<?php

namespace app\widgets;

use yii\base\Widget;
use app\models\Category;

class Categories extends Widget {

  function run()
  {
    $categories = Category::find()->all();
    return $this->render('categories', compact ('categories'));
  }
}
?>
