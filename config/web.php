<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'name'=>'Prohorweb',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'VJ7ZAO-kv5jtWQtqM5tGoPTVfNHuk8DF',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['auth/login']
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],

    ],
    'modules' => [
      'admin' => [
          'class' => 'app\modules\admin\Module',
    ],
  ],

  'controllerMap' => [
      'elfinder' => [
          'class' => 'mihaildev\elfinder\Controller',
          'access' => ['@'], //глобальный доступ к фаил менеджеру @ - для авторизорованных , ? - для гостей , чтоб открыть всем ['@', '?']
          'disabledCommands' => ['netmount'], //отключение ненужных команд https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#commands
          'roots' => [
              [
                  'baseUrl'=>'@web',
                  'basePath'=>'@webroot',
                  'path' => 'uploads',
                  'name' => 'Global',
                  
              ],
              // [
              //     'class' => 'mihaildev\elfinder\volume\UserPath',
              //     'path'  => 'files/user_{id}',
              //     'name'  => 'My Documents'
              // ],
              // [
              //     'path' => 'files/some',
              //     'name' => ['category' => 'my','message' => 'Some Name'] //перевод Yii::t($category, $message)
              // ],
              // [
              //     'path'   => 'files/some',
              //     'name'   => ['category' => 'my','message' => 'Some Name'], // Yii::t($category, $message)
              //     'access' => ['read' => '*', 'write' => 'UserFilesAccess'] // * - для всех, иначе проверка доступа в даааном примере все могут видет а редактировать могут пользователи только с правами UserFilesAccess
              // ]
          ],
      ]
  ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
