<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use vova07\imperavi\Widget;
use dosamigos\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(['id' => 'my-form']); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>



        <?= $form->field($model, 'content')->widget(CKEditor::className(), [
            'preset' => 'custom',
            'options' => ['rows' => 15],
            'clientOptions' => ElFinder::ckeditorOptions('elfinder',[
                           'toolbarGroups' => [
                              ['name' => 'document', 'groups' => ['mode', 'document', 'doctools']],
                              ['name' => 'clipboard', 'groups' => ['clipboard', 'undo']],
                              // ['name' => 'editing', 'groups' => [ 'find', 'selection', 'spellchecker']],
                              // ['name' => 'forms'],
                              ['name' => 'basicstyles', 'groups' => ['basicstyles', 'colors','cleanup']],
                              ['name' => 'paragraph', 'groups' => [ 'list', 'indent', 'blocks', 'align', 'bidi' ]],
                              ['name' => 'links'],
                              ['name' => 'insert'],
                              ['name' => 'styles'],
                              ['name' => 'blocks'],
                              // ['name' => 'colors'],
                              ['name' => 'tools'],
                              // ['name' => 'others'],
                            ]
                          ])
    ]) ?>
    <?= $form->field($model, 'date')->textInput() ?>

    <!-- <?= $form->field($model, 'viewed')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'category_id')->textInput() ?>  -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
