<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageUpload extends Model{

    public $image; //загруженный файл доступный вовсе классе

    public function rules()
    {
      return [
          [['image'], 'required'],
          [['image'], 'file', 'extensions' => 'jpg,png']
      ];
    }

    public function uploadFile(UploadedFile $file, $currentImage)
    {

      $this->image = $file;//присваиваем название файла из колонки БД для доступности во всем классе

      if ($this->validate()){

        $this->deleteCurrentImage($currentImage);//удаление текущего файла
        return $this->saveImage();//сохранение нового файла

      }
    }

    private function getFolder() // папка загрузки
    {
      return Yii::getAlias('@web'). 'uploads/';
    }

    private function generateFilename() // генерация уникального имени
    {
      return strtolower(uniqid($this->image->baseName).'.'. $this->image->extension);
    }

    public function deleteCurrentImage($currentImage) //удаление текущего файла
    {
      if($this->fileExists($currentImage))
      {
          unlink($this->getFolder() . $currentImage);
      }
    }

    public function fileExists($currentImage)//проверка на существование файла
    {
      if(!empty($currentImage) && $currentImage != null)
     {
         return file_exists($this->getFolder() . $currentImage);
     }
    }

    public function saveImage() //возвращение названия картинки
    {
      $filename = $this->generateFilename();//уникализация файла
      $this->image->saveAs($this->getFolder() . $filename);// сохранение файла в папку
      return $filename;
    }


}
